<?php

/**
 *  Gmap
 *
 *  @package Bludit
 *  @subpackage Plugins
 *  @author PB-Soft - Patrick Biegel
 *  @copyright 2018 PB-Soft
 *  @version 2.0
 *  @release 2018-12-10
 *
 */

class pluginGmap extends Plugin {

  // Method to initialize the plugin.
  public function init()
  {

    // Specify the used database fields.
    $this->dbFields = array(
      'map_height'    => '', // Height of the DIV element that contains the map.
      'map_width'     => '', // Width of the DIV element that contains the map.
      'map_page'      => '', // Page name on which to include the map.
      'map_zoom'      => '', // Zoom setting for the map.
      'marker_title'  => '', // Title for the marker on the map.
      'location_lat'  => '', // Latitude of the location  --> Check: https://support.google.com/maps/answer/18539
      'location_lng'  => '', // Longitude of the location --> Check: https://support.google.com/maps/answer/18539
      'map_api_key'   => '', // Google API key            --> Check: https://developers.google.com/maps/documentation/javascript/get-api-key
      'map_emb_str'   => ''  // Google map embed string   --> Check: https://support.google.com/maps/answer/144361
    );
  }

  // Administration page to edit the settings.
  public function form()
  {

    // Set the language variable global.
    global $Language;

    // Map height input field.
    HTML::formInputText(array(
      'name'        => 'map_height',
      'label'       => $Language->get('map-height'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_height'),
      'placeholder' => '400px',
      'tip'         => $Language->get('map-height-tip'),
      'disabled'    => false
    ));

    // Map width input field.
    HTML::formInputText(array(
      'name'        => 'map_width',
      'label'       => $Language->get('map-width'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_width'),
      'placeholder' => '100%',
      'tip'         => $Language->get('map-width-tip'),
      'disabled'    => false
    ));

    // Map page input field.
    HTML::formInputText(array(
      'name'        => 'map_page',
      'label'       => $Language->get('map-page'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_page'),
      'placeholder' => $Language->get('map-page-placeholder'),
      'tip'         => $Language->get('map-page-tip'),
      'disabled'    => false
    ));

    // Map zoom input field.
    HTML::formInputText(array(
      'name'        => 'map_zoom',
      'label'       => $Language->get('map-zoom'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_zoom'),
      'placeholder' => '16',
      'tip'         => $Language->get('map-zoom-tip'),
      'disabled'    => false
    ));

    // Marker title input field.
    HTML::formInputText(array(
      'name'        => 'marker_title',
      'label'       => $Language->get('marker-title'),
      'type'        => 'text',
      'value'       => $this->getDbField('marker_title'),
      'placeholder' => $Language->get('marker-title-placeholder'),
      'tip'         => $Language->get('marker-title-tip'),
      'disabled'    => false
    ));

    // Location latitude input field.
    HTML::formInputText(array(
      'name'        => 'location_lat',
      'label'       => $Language->get('location-lat'),
      'type'        => 'text',
      'value'       => $this->getDbField('location_lat'),
      'placeholder' => '47.500978',
      'tip'         => $Language->get('location-lat-tip'),
      'disabled'    => false
    ));

    // Location longitude input field.
    HTML::formInputText(array(
      'name'        => 'location_lng',
      'label'       => $Language->get('location-lng'),
      'type'        => 'text',
      'value'       => $this->getDbField('location_lng'),
      'placeholder' => '8.724103',
      'tip'         => $Language->get('location-lng-tip'),
      'disabled'    => false
    ));

    // Google map API key input field.
    HTML::formInputText(array(
      'name'        => 'map_api_key',
      'label'       => $Language->get('map-api-key'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_api_key'),
      'placeholder' => 'XYzaBcDefGHij6...',
      'tip'         => $Language->get('map-api-key-tip'),
      'disabled'    => false
    ));

    // Google embed string input field.
    HTML::formInputText(array(
      'name'        => 'map_emb_str',
      'label'       => $Language->get('map-emb-str'),
      'type'        => 'text',
      'value'       => $this->getDbField('map_emb_str'),
      'placeholder' => '!1m16!2m35!1m4...',
      'tip'         => $Language->get('map-emb-str-tip'),
      'disabled'    => false
    ));
  }

  // Function to start the output buffering and get the page content.
  public function pageBegin()
  {

    // Specify the global variables.
    global $WHERE_AM_I, $page;

    // Check if the actual page is the specified page to insert the map.
    if (isset($WHERE_AM_I) && $WHERE_AM_I == 'page' && $page->slug() === $this->getDbField('map_page'))
    {

      // Start the output buffering.
      ob_start();
    }
  }

  // Function to insert the map into the page content.
  public function pageEnd()
  {

    // Specify the global variables.
    global $WHERE_AM_I, $page;

    // Check if the actual page is the specified page to insert the map.
    if (isset($WHERE_AM_I) && $WHERE_AM_I == 'page' && $page->slug() === $this->getDbField('map_page'))
    {

      // Get the page content into a variable.
      $page_content = ob_get_clean();

      // Get the data from the database.
      $map_height   = $this->getDbField('map_height');
      $map_width    = $this->getDbField('map_width');
      $map_zoom     = $this->getDbField('map_zoom');
      $marker_title = $this->getDbField('marker_title');
      $location_lat = $this->getDbField('location_lat');
      $location_lng = $this->getDbField('location_lng');
      $map_api_key  = $this->getDbField('map_api_key');
      $map_emb_str  = $this->getDbField('map_emb_str');

      // Start the output buffering to get the map.
      ob_start();

      // Include the map template.
      require(__DIR__.DS.'template'.DS.'map.php');

      // Get the map code from the buffer.
      $map_code = ob_get_clean();

      // Remove the unnecessary paragraph tags around the placeholder.
      $page_content = str_ireplace("<p>[GOOGLEMAP]</p>", "[GOOGLEMAP]", $page_content);

      // Search for the map placeholder.
      $position = stripos($page_content, "[GOOGLEMAP]");

      // Check if the map placeholder could be found.
      if ($position !== false) {

        // Replace only the first occurrence of the placeholder with the map.
        $page_content = substr_replace($page_content, $map_code, $position, strlen("[GOOGLEMAP]"));

        // Remove all other placeholders.
        $page_content = str_replace("[GOOGLEMAP]", "", $page_content);

        // The map placeholder could not be found.
      } else {

        // Include the map at the end of the page.
        $page_content .= $map_code;
      }

      // Display the changed page content.
      echo $page_content;
    }
  }
}
