<?php

/**
 *  Gmap
 *
 *  @package Bludit
 *  @subpackage Plugins
 *  @author PB-Soft - Patrick Biegel
 *  @copyright 2018 PB-Soft
 *  @version 3.0
 *  @release 2018-12-10
 *
 */

class pluginGmap extends Plugin {

  // Method to initialize the plugin.
  public function init()
  {

    // Specify the used database fields.
    $this->dbFields = array(
      'map_height'    => '', // Height of the DIV element that contains the map.
      'map_width'     => '', // Width of the DIV element that contains the map.
      'map_page'      => '', // Page name on which to include the map.
      'map_zoom'      => '', // Zoom setting for the map.
      'marker_title'  => '', // Title for the marker on the map.
      'location_lat'  => '', // Latitude of the location  --> Check: https://support.google.com/maps/answer/18539
      'location_lng'  => '', // Longitude of the location --> Check: https://support.google.com/maps/answer/18539
      'map_api_key'   => '', // Google API key            --> Check: https://developers.google.com/maps/documentation/javascript/get-api-key
      'map_emb_str'   => ''  // Google map embed string   --> Check: https://support.google.com/maps/answer/144361
    );
  }

  // Administration page to edit the settings.
  public function form()
  {

    // Set the language variable global.
    global $L;

    // Map height input field.
    $html  = '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-height').'</label>';
    $html .= '<input name="map_height" id="map_height" type="text" placeholder="400px" value="'.$this->getValue('map_height').'">';
    $html .= '<p>'.$L->get('map-height-tip').'</p>';
    $html .= '</div>';

    // Map width input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-width').'</label>';
    $html .= '<input name="map_width" id="map_width" type="text" placeholder="100%" value="'.$this->getValue('map_width').'">';
    $html .= '<p>'.$L->get('map-width-tip').'</p>';
    $html .= '</div>';

    // Map page input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-page').'</label>';
    $html .= '<input name="map_page" id="map_page" type="text" placeholder="'.$L->get('map-page-placeholder').'" value="'.$this->getValue('map_page').'">';
    $html .= '<p>'.$L->get('map-page-tip').'</p>';
    $html .= '</div>';

    // Map zoom input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-zoom').'</label>';
    $html .= '<input name="map_zoom" id="map_zoom" type="text" placeholder="16" value="'.$this->getValue('map_zoom').'">';
    $html .= '<p>'.$L->get('map-zoom-tip').'</p>';
    $html .= '</div>';

    // Marker title input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('marker-title').'</label>';
    $html .= '<input name="marker_title" id="marker_title" type="text" placeholder="'.$L->get('marker-title-placeholder').'" value="'.$this->getValue('marker_title').'">';
    $html .= '<p>'.$L->get('marker-title-tip').'</p>';
    $html .= '</div>';

    // Location latitude input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('location-lat').'</label>';
    $html .= '<input name="location_lat" id="location_lat" type="text" placeholder="47.500978" value="'.$this->getValue('location_lat').'">';
    $html .= '<p>'.$L->get('location-lat-tip').'</p>';
    $html .= '</div>';

    // Location longitude input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('location-lng').'</label>';
    $html .= '<input name="location_lng" id="location_lng" type="text" placeholder="8.724103" value="'.$this->getValue('location_lng').'">';
    $html .= '<p>'.$L->get('location-lng-tip').'</p>';
    $html .= '</div>';

    // Google map API key input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-api-key').'</label>';
    $html .= '<input name="map_api_key" id="map_api_key" type="text" placeholder="XYzaBcDefGHij6..." value="'.$this->getValue('map_api_key').'">';
    $html .= '<p>'.$L->get('map-api-key-tip').'</p>';
    $html .= '</div>';

    // Google embed string input field.
    $html .= '<div>';
    $html .= '<label style="font-weight:bold">'.$L->get('map-emb-str').'</label>';
    $html .= '<input name="map_emb_str" id="map_emb_str" type="text" placeholder="!1m16!2m35!1m4..." value="'.$this->getValue('map_emb_str').'">';
    $html .= '<p>'.$L->get('map-emb-str-tip').'</p>';
    $html .= '</div>';

    return $html;
  }

  // Function to start the output buffering and get the page content.
  public function pageBegin()
  {

    // Specify the global variables.
    global $WHERE_AM_I, $page;

    // Check if the actual page is the specified page to insert the map.
    if (isset($WHERE_AM_I) && $WHERE_AM_I == 'page' && $page->slug() === $this->getValue('map_page'))
    {

      // Start the output buffering.
      ob_start();
    }
  }

  // Function to insert the map into the page content.
  public function pageEnd()
  {

    // Specify the global variables.
    global $WHERE_AM_I, $page;

    // Check if the actual page is the specified page to insert the map.
    if (isset($WHERE_AM_I) && $WHERE_AM_I == 'page' && $page->slug() === $this->getValue('map_page'))
    {

      // Get the page content into a variable.
      $page_content = ob_get_clean();

      // Get the data from the database.
      $map_height   = $this->getValue('map_height');
      $map_width    = $this->getValue('map_width');
      $map_zoom     = $this->getValue('map_zoom');
      $marker_title = $this->getValue('marker_title');
      $location_lat = $this->getValue('location_lat');
      $location_lng = $this->getValue('location_lng');
      $map_api_key  = $this->getValue('map_api_key');
      $map_emb_str  = $this->getValue('map_emb_str');

      // Start the output buffering to get the map.
      ob_start();

      // Include the map template.
      require(__DIR__.DS.'template'.DS.'map.php');

      // Get the map code from the buffer.
      $map_code = ob_get_clean();

      // Remove the unnecessary paragraph tags around the placeholder.
      $page_content = str_ireplace("<p>[GOOGLEMAP]</p>", "[GOOGLEMAP]", $page_content);

      // Search for the map placeholder.
      $position = stripos($page_content, "[GOOGLEMAP]");

      // Check if the map placeholder could be found.
      if ($position !== false) {

        // Replace only the first occurrence of the placeholder with the map.
        $page_content = substr_replace($page_content, $map_code, $position, strlen("[GOOGLEMAP]"));

        // Remove all other placeholders.
        $page_content = str_replace("[GOOGLEMAP]", "", $page_content);

        // The map placeholder could not be found.
      } else {

        // Include the map at the end of the page.
        $page_content .= $map_code;
      }

      // Display the changed page content.
      echo $page_content;
    }
  }
}
