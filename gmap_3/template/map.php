<?php

/**
 *  Gmaps
 *
 *  @package Bludit
 *  @subpackage Plugins
 *  @author PB-Soft - Patrick Biegel
 *  @copyright 2018 PB-Soft
 *  @version 3.0
 *  @release 2018-08-20
 *  @update 2018-12-10
 *
 */

// Check if a Google API key is available.
if ($map_api_key != "") {

?>

  <!-- Add the DIV element for the map. -->
  <div id="map" style="height: <?php echo $map_height; ?>; width: <?php echo $map_width; ?>;"></div>

  <!-- Script to create the map and marker. -->
  <script>

    // Initialize and add the map.
    function initMap() {

      // Specify location of the map.
      var location = {lat: <?php echo $location_lat; ?>, lng: <?php echo $location_lng; ?>};

      // Create a new map and center it at the specified location.
      var map = new google.maps.Map(document.getElementById('map'), {center: location, zoom: <?php echo $map_zoom; ?>});

      // Set the marker to the specified location.
      var marker = new google.maps.Marker({map: map, position: location, title: '<?php echo $marker_title; ?>'});
    }

  </script>

  <!-- Include the external maps script. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $map_api_key; ?>&callback=initMap" async defer>
  </script>

<?php

// Check if the Google embedding string is available.
} elseif ($map_emb_str != "") {

?>

  <!-- Insert the map with the help of the embeding string. -->
  <iframe src="https://www.google.com/maps/embed?pb=<?php echo $map_emb_str; ?>" width="<?php echo $map_width; ?>" height="<?php echo $map_height; ?>" frameborder="0" style="border:0" allowfullscreen></iframe>

<?php

}

?>
