# Gmap Plugin - README #
---

### Overview ###

**Gmap** is a plugin for [**Bludit**](https://www.bludit.com/), a nice little and slim CMS written in PHP. This Plugin can display a GoogleMap on a specified CMS page. If you use the placeholder/shortcode [GOOGLEMAP], you can position the map between the text paragraphs on the page. If you do not use the placeholder and only specify the name of the page in the settings, the map will be placed at the end of the specified page. On the plugin settings page, you can specify the map size, map location, the title of the marker and also your Google API key. If you do not want to use a Google API key, you can let the APi key input field empty and enter an Google embed string in the field below.

### Screenshots ###

![Gmap - Contact page](development/readme/gmap_1.png "Gmap - Contact page")
![Gmap - Plugin settings](development/readme/gmap_2.png "Gmap - Plugin settings")

### Setup ###

* Copy the directory **gmap** to the plugin directory of your Bludit installation.
* Normally this is under **bludit/bl-plugins/**.
* The plugin folder would be under **bludit/bl-plugins/gmap**.
* Go to the Bludit administration page and select **Plugins** in the left sidebar.
* You should see the plugin installed there under the name **Gmap**.
* Click on the link below the plugin name to enable the plugin.
* Click on the link **Settings** to enter all the data for creating the map.
* If you want, enter the placeholder/shortcode [GOOGLEMAP] on one of your pages.
* If you do not want to use the Google API key, enter an embed string in the field below.

### Additional Information ###

* If you want to know how to get a Google Maps API key, check the following link:
[https://developers.google.com/maps/documentation/javascript/get-api-key](https://developers.google.com/maps/documentation/javascript/get-api-key)

* If you want to know how to get the latitude/longitude of your location, check the following link:
[https://support.google.com/maps/answer/18539](https://support.google.com/maps/answer/18539)

* If you want to know how to get a Google Maps embed string, check the following link:
[https://support.google.com/maps/answer/144361](https://support.google.com/maps/answer/144361)

* If you get the HTML code to embed a Google Map, it will look similar to the following one:

``` html
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d97173.9784095991!2d-3.7497474130353936!3d40.43793306292252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd422997800a3c81%3A0xc436dec1618c2269!2sMadrid%2C+Spain!5e0!3m2!1sen!2sch!4v1534802461840" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
```
* If you want to use the embed string, you have to enter only the URL part after the **pb=?** characters.
* In the example above this would be ONLY the following:

``` html
!1m18!1m12!1m3!1d97173.9784095991!2d-3.7497474130353936!3d40.43793306292252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd422997800a3c81%3A0xc436dec1618c2269!2sMadrid%2C+Spain!5e0!3m2!1sen!2sch!4v1534802461840
```

### Troubleshooting ###
If the plugin does not work and no Google map is displayed, please check the following:
* Is your **Bludit** version at least version 2.3.4?
* You can get the newest version here: [Bludit Website](https://www.bludit.com/)
* Is the plugin working with the default themes **Alternative** or **Blog X**?
* Does your theme uses the hook **pageBegin** before the page content is displayed?
* Does your theme uses the hook **pageEnd** after the page content is displayed?
* This two hooks are necessary (essential) for the plugin to work.
* The hooks would look similar to the following in the file **bludit/bl-themes/blogx/page.php**:

``` php
<!-- Load Bludit Plugins: Page Begin -->
<?php Theme::plugins('pageBegin'); ?>

  // The content of the page is displayed here...

<!-- Load Bludit Plugins: Page End -->
<?php Theme::plugins('pageEnd'); ?>
```

### Support ###

This is a free plugin and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Gmap** plugin is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
